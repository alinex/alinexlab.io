# Log/Events

## Log/Event Analyzation

A well known solution is the ELK-Stack which brings ElasticSearch together with Logstash and Kibana to form a complete solution. Alternatives here are Graylog or a central rsyslog server.

![Events Architecture](events.svg)

The Beats clients on the hosts will send log entries to Logstash. They will be parsed into events and stored in ElasticSearch Database for further analysis. Kibana builds the web frontend to easily query and display this data. Also they could be added to Grafana boards.

- centralized log information
- search and filtering through all logs
- easy analyzation
- alerts on problems

{!docs/assets/abbreviations.txt!}
