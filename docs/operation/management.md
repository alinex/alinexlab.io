title: Management

## Operations Management

This area is a toolset to help the operator do his work. It is highly individual and specific for the software system so there is no general tool. Only a framework may help here.

![Management Architecture](management.svg)

An Analyzer will find and step over hosts and analyze host, process, service and batches. This will be stored in an inventory database for further use. The inventory will also produce human redable documentation in confluence, and configure the metrics collection of Prometheus. A Maintenance System is the direct CLI helper for the administrator to do some tasks with support. It can run different actors which will analyze, report and repair systems. Therefore the information from inventory and the monitoring systems can be used.

In an framework for easy prototyping it may:

- document system
- keep changes over time
- assist operator in his tasks
- make analysis and actions over multiple hosts


{!docs/assets/abbreviations.txt!}
