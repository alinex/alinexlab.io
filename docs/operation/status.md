title: Status

# Status Monitoring

A lot of tools are usable here: Zabbix, PRTG, Nagios, Icinga and many more.
In addition some user simulations like Selenium or TestCafe can help.

![Monitoring Architecture](status.svg)

We will use one of the above systems to monitor systems, gather actual state and alert on it. Often the system is based on what the data center uses and if systems are spanned over multiple data senters you may also use different such systems together.
Additionally to simple tests, Selenium or TestCafe is used for additional in deep user simulations which should be triggered not to often because of their complexity.

The status monitoring will help you with:

- alerts if something won't work correctly
- outage reports
- graphical outage map (i.e. Zabbix)
- QOS calculation
- long time storage of these information
- preproduction test if used in staging

{!docs/assets/abbreviations.txt!}
