title: Overview

![bash-lib icon](../assets/icons/tux-icon.png){: .right .icon}

# Linux

As you see on all these pages I work mostly on Linux systems. In this chapter I collect a lot of knowledge around the operating system and it's tools.

It is by far not complete but a collection of useful information.

## Distributions

At first then discussing about Linux you have to look at the distribution used. There are general differences between them but the core is always Linux.
Also some distributions are build on each other, making them similar.

<!-- prettier-ignore -->
blockdiag {
   Debian [color = orange];
   Ubuntu [color = wheat];
   Kubuntu [color = wheat];
   "Linux Mint" [color = wheat];
   Manjaro [color = orange];
   Alpine [color = orange];
   Slackware [color = wheat];
   SuSE [color = wheat];
   Gentoo [color = wheat];
   CentOS [color = orange];
   OSX [color = wheat];
   Solaris [color = wheat];
   // history
   "GNU Linux" -> Debian;
   Debian -> Ubuntu [thick];
   Ubuntu -> Kubuntu [thick];
   Ubuntu -> "Linux Mint" [thick];
   Ubuntu -> "Chromium OS" [thick];
   "GNU Linux" -> "Arch Linux";
   "Arch Linux" -> Manjaro [thick];
   "GNU Linux" -> Alpine;
   "GNU Linux" -> RedHat -> Fedora -> RHEL;
   RHEL -> CentOS [thick];
   "GNU Linux" -> Slackware -> SuSE;
   "GNU Linux" -> Gentoo;
   BSD -> Solaris;
   BSD -> NextStep -> OSX;
}

You see here some of the distributions with their ancestors. A bold arrow means it is not only derrived but also based on the previous one. The coloring shows what I worked on (light orange) or are used by me currently (orange).

The base distribution mostly defines the used package manager:

Debian

: is a release based free OS lead by community and using APT for it's own debian package format. It is the most used distribution, really stable but tends to be a bit out of date.
Derivates are Ubuntu (with Kubutnu, Lubuntu, Xubuntu), Mint, KDE Neon, Elementary OS, Knoppix, Chromium OS.

Arch

: uses rolling releases and is build only for x86-64 with simplicity, modernity, pragmatism, user centrality, and versatility as key principles. This tends to have minimal distribution-specific changes, minimal breakage with updates and pragmatic over ideological design choices. The package manager is called Pacman which works with compressed archives and yay to build from user repository.
Derivates are Manjaro, Chakra.

RedHat

: itself is a commercial enterprise Linux with YUM to manage RPM packages.
But it's derivates like Mandrake/Mandriva, Fedora and CentOS are free.

SUSE Linux

: was originally build from Slackware by a german team. It incorporated many aspects of RedHat and also builds on RPM packages and is released as enterprise and open source community edition.

Gentoo

: is a source based distribution and is compiled locally according to the user's preferences and is often optimized for the specific type of computer. Precompiled binaries are available for some larger packages or those with no available source code. Portage is used for package management.

Alpine

: is designed for security, simplicity, and resource efficiency. Because of its small size, it is commonly used in containers providing quick boot-up times.

## Desktops

Beside the distribution the main selection is the desktop environment. There are multiple possibilities within the distributions and often you may select ione by yourself within them.

KDE Plasma

: very configurable, for experienced users, graphical effects and used as default in Kubuntu, OpenSuse and Fedora

Gnome 3

: simple, for newbies, limited configuration and used as default in Ubuntu and Fedora

Cinnamon

: classical desktop, configurable and developed and used in Linux Mint

Mate

: simple, for newbies, low resources needed

XFCE

: running also on older hardware and is used in Xubuntu

LXDE

: for low resources, configurable and used in Lubuntu

{!docs/assets/abbreviations.txt!}
