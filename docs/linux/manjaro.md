title: Arch Linux

# Manjaro KDE based on Arch Linux

This is a short overview what I installed and configured on my new laptop (2020).

!!! info

    I use a HP EliteBook 640G but will not go in detail about hardware settings.
    The following is based on my personal needs.

## OS Installation

To install I had to disable UEFI and switch the BIOS into legacy mode. After that booting with a bootable USB stick (used unetbootin to create it) I could start the installation. I installed Manjaro KDE 20.2.

### Sudo without password

Add a new file for user allowance:

```text title="sudo vi /etc/sudoers.d/50-user"
alex ALL=(ALL) NOPASSWD: ALL
```

### Fix Audio

To enable audio I had to install the following packages:

```bash
$ sudo pacman -S sof-firmware alsa-ucm-conf
```

### Fix Brightness Keys

To make the fn brightness keys working use the vendor based drivers instead of the default ones.

Open the file `/etc/default/grub` and change the below line:

```text
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
# change above line to:
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor"
```

Now update the grub boot loader and reboot:

```bash
$ sudo update-grub
$ sudo shutdown -r now
```

### Fix pkgfile

When I open my laptop, before loading the SDDM, the screen will say:

    [FAILED] failed to start pkgfile database update

I checked there this is used:

```bash
$ pactree -r pkgfile
pkgfile
└─manjaro-zsh-config
```

And then fixed it by initializing pkgfile/:

```bash
$ sudo pkgfile -u
:: Updating 4 repos...
warning: download failed: http://distro.ibiblio.org/manjaro/stable/core/x86_64/core.files [error 403]
  download complete: multilib             [   262,5 KiB   297K/s  3 remaining]
  download complete: extra                [     9,4 MiB  2,60M/s  2 remaining]
  download complete: core                 [  1578,9 KiB   361K/s  1 remaining]
  download complete: community            [    29,1 MiB  1910K/s  0 remaining]
:: download complete in 15,59s            <    40,3 MiB  2,58M/s  4 files    >
:: waiting for 1 process to finish repacking repos...
```

### Swap

In most circumstances swap is not absolutely required unless you are running out of memory. However, even with plenty of available memory, it is often used as a safety net or due to specific application requirements. So as I at first didn't setup a separate partition I would like to add one now.

!!! Info

    Without a swap space >= memory size the option to suspend to disk is not available.

As I have enough disk space I decided to add one with the size a bit larger then memory. But as I has no space reserved for a new partition I decided to make it within the existing partition (possible with the newer kernels). This can also be done in addition to a swap partition.

First to check the current swap:

```bash
$ swapon        # show the current swap devices
```

As nothing is shown there is no swap space. To create one as file use:

```bash
$ sudo fallocate -l 20G /swapfile   # create swap file
$ sudo chmod 600 /swapfile          # restrict access
$ sudo mkswap /swapfile             # define as swap
$ sudo swapon /swapfile             # activate
# and to also get it attached after restart:
$ echo "/swapfile none swap defaults 0 0" | sudo tee -a /etc/fstab
```

Now you will see the added swap device.

```bash
$ swapon
NAME      TYPE SIZE USED PRIO
/swapfile file  20G   0B   -2
```

To see if the suspend to disk is working you have to reboot first.

## Software Installation

I added the following applications, which I installed with the pamac-manager (graphical installation):

-   LibreOffice
-   Gimp
-   peek
-   Terminator
-   KeePassXC
-   Calibre
-   curlftpfs
-   vim
-   openssh
-   npm
-   jq
-   postgresql
-   vsftpd
-   bind (host, nslookup, dig)
-   FileZilla
-   DBeaver

And then I enabled AUR (settings within the pamac) to install from user repositories:

-   vi-vim-symlink
-   google-chrome
-   visual-studio-code-bin
-   yed
-   mongodb-bin
-   mongodb-tools-bin
-   mongodb-compass

I also removed things I don't use:

-   thunderbird

### pacman

The pacman package manager combines a simple binary package format with an easy-to-use build system. The goal of pacman is to make it possible to easily manage packages, whether they are from the official repositories or the user's own builds.

Pacman keeps the system up to date by synchronizing package lists with the master server. This server/client model also allows the user to download/install packages with a simple command, complete with all required dependencies.

A package is an archive containing:

-   all of the (compiled) files of an application
-   metadata about the application, such as application name, version, dependencies, ...
-   installation files and directives for pacman
-   (optionally) extra files to make your life easier, such as a start/stop script

Usage: `pacman <command> <package> [<package>...]`

**Install commands**

```bash
# Update package list
sudo pacman -Sy
# Update package list (refresh also if up to date)
sudo pacman -Syy

# Update package list and update all
sudo pacman -Syu
# Update package list and update/install
sudo pacman -Syu <package>

# Remove package
sudo pacman -Rsc <package>
```

**Querying**

```bash
# List explictly-installed packages
pacman -Qe
# List all installed packages
pacman -Qqe
# List installed AUR packages
pacman -Qqm

# List packages which can be upgraded
checkupdates
# Search for packages
pacman -Ss <keywords>

# What files does this package have?
pacman -Ql <package>
# List information on package
pacman -Qii <package>

# Who owns this file?
pacman -Qo <file>
# Search installed packages for keywords
pacman -Qs <keyword>
```

**Orphans**

```bash
# List orphaned packages which may be removed
pacman -Qtd
# Uninstall unneeded packages
sudo pacman -Rns $(pacman -Qdtq)
```

**Dependencies**

```bash
# What does pkg depend on?
pactree <package>
# What depends on package?
pactree -r <package>
```

Find more commands under [pacman/Tips and tricks](https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks).

### yay

To install packages from the AUR you have to build the package first. There are different tools which will do this for you. One of it is `yay`.

Install it using:

```bash
$ git clone https://aur.archlinux.org/yay-git.git
$ cd yay-git
$ makepkg -si
```

And then use it like pacman:

```bash
$ yay -S yed
```

## Setup

### Klipper

**Problem:** When I mark a text with the mouse, it’s automatically copied to the clipboard. But you want to use different copy/paste clipboard and select/middle click.

**Solution:** You can configure this. Open the settings from the "Clipboard" application (maybe added to the desktop first) and set General > Selection and Clipboard > Ignore selection.

### Vim

```text title="~/.vimrc"
set mouse=v
syntax on
```

### Bash completion

Tab completion was not running correctly if using sudo.

```bash title=".bashrc"
# The following has to be deactivated to be replaced with a better function below
#complete -cf sudo

# completion.bash
_vault_complete() {
    COMPREPLY=()
    local word="${COMP_WORDS[COMP_CWORD]}"
    local completions="$(vault --cmplt "$word")"
    COMPREPLY=( $(compgen -W "$completions" -- "$word") )
}
complete -f -F _vault_complete vault

export PATH=$PATH:/home/alex/bin
```

This will complete all your commands with infinite possibilities. See the description of [James Coglan](https://blog.jcoglan.com/2013/02/12/tab-completion-for-your-command-line-apps/).

I also added the binaries under my homedir.

### SSH

I copied my ssh keys (folder `.ssh`) from my previous server directly.

Further to not get a disconnect on long running sessions without data change set a keep alive ping in `/etc/ssh_config`:

    ServerAliveInterval 120

### Git

I had to [setup login](../env/git.md#installation) first with user name and email.

### Code Plugins:

See description under [Code](../env/vscode.md) for detailed description of:

-   [Git Extension Pack](https://open-vsx.org/extension/donjayamanne/git-extension-pack)
-   [Markdown All in One](https://open-vsx.org/vscode/item?itemName=yzhang.markdown-all-in-one)
-   [Code Spell Checker](https://open-vsx.org/extension/streetsidesoftware/code-spell-checker) with dictionaries like German
-   [Peacock](https://open-vsx.org/extension/johnpapa/vscode-peacock)
-   [todo-tree](https://open-vsx.org/extension/Gruntfuggly/todo-tree)
-   [VueJS Extension Pack](https://marketplace.visualstudio.com/items?itemName=mubaidr.vuejs-extension-pack)
-   [REST Client](https://open-vsx.org/extension/humao/rest-client)
-   [Remote SSH](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh)

### Firefox

Under settings enable Firefox Sync if used to get the same plugins and bookmarks on all your devices.

### KeePassXC

As described under [KeePass](../env/keepass.md) I had to mount the ftp shares containing my private and work KeePass databases.

-   /mnt/ftp-fritzbox
-   /mnt/ftp-itbetrieb

### Middleware

Some middleware is not needed everyday so I disable it generally:

```bash
$ sudo systemctl disable mongo.service
```

And start it then needed (I made a script for it to start all such development/test services):

```bash
$ sudo systemctl start sshd.service
$ sudo systemctl start mongo.service
```

{!docs/assets/abbreviations.txt!}
