title: screen

# Screen - independent terminal session

To work with long running scripts `nohup` may be used. But to have better and further control `screen` will help. It also allows to share the terminal session.

## Installation

=== "Arch Linux"

    ```bash
    $ sudo pacman -S screen
    ```

=== "Debian/Ubuntu"

    ```bash
    $ sudo apt update
    $ sudo apt install screen
    ```

## Start screen

Start a new session:

```bash
$ screen      # new default session
$ screen -S my_name # named session
```

Named sessions are useful when you run multiple screen sessions.

## Working in screen

Within the screen the following commands will help:

- ++ctrl+a++ and ++question++ Show all commands
- ++ctrl+a++ and ++d++ will detach from screen and you may close the terminal

Scrolling in output buffer can be done using:

- ++ctrl+a++ and ++esc++ to switch mode
- then ++up++ and ++down++ to navigate
- and ++esc++ to end this mode

## Windows

When you start a new screen session, it creates a single window with a shell in it.
But multiple windows inside a Screen are possible (with numbers 0..9).

- ++ctrl+a++ and ++c++ Create a new window (with shell)
- ++ctrl+a++ and ++double-quote++ List all window
- ++ctrl+a++ and ++space++ Switch between windows
- ++ctrl+a++ and ++n++ Switch to next window
- ++ctrl+a++ and ++p++ Switch to previous window
- ++ctrl+a++ and ++0++...++9++ Switch to window by number
- ++ctrl+a++ and ++a++ Rename the current window
- ++ctrl+a++ and ++s++ Split current region horizontally into two regions
- ++ctrl+a++ and ++pipe++ Split current region vertically into two regions
- ++ctrl+a++ and ++tab++ Switch the input focus to the next region
- ++ctrl+a++ and ++ctrl+a++ Toggle between the current and previous region
- ++ctrl+a++ and ++k++ Close the current window
- ++ctrl+a++ and ++q++ Close all regions but the current one
- ++ctrl+a++ and ++x++ Close the current region

## Resume screen

And to jump into a running screen use:

```bash
$ screen -r   # reattach to default session
$ screen -ls  # show list of screens with numbers
$ screen -r <num> # reattach to numbered entry
```

## Delete screen

From the outside a screen can be deleted using:

```bash
$ screen -d <num>
```

{!docs/assets/abbreviations.txt!}
