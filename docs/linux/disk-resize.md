# Disk Resize

## Increase Disk Space

If it is a virtual machine, and this is the last partition on the disk it is easy.

1. Increase the disk size for the virtual machine.
   
2. `lsblk`  will show that there is some free space (here 5GB)

    ```text hl_lines="8 9"
    $ lsblk
    NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
    fd0      2:0    1    4K  0 disk 
    sda      8:0    0   20G  0 disk 
    ├─sda1   8:1    0 19,1G  0 part /
    ├─sda2   8:2    0    1K  0 part 
    └─sda5   8:5    0  880M  0 part [SWAP]
    sdb      8:16   0   15G  0 disk 
    └─sdb1   8:17   0   10G  0 part /mnt1
    sr0     11:0    1 1024M  0 rom  
    ```

3.  Install parted if not already done:

    === "Debian/Ubuntu"

        ```bash
        sudo apt-get -y install parted
        ```

4.  Now you show the details of the disk:
   

    ```bash
    $ sudo parted /dev/sdb print free
    Model: VMware Virtual disk (scsi)
    Disk /dev/sdb: 16,1GB
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags: 

    Number  Start   End     Size    Type     File system  Flags
            1024B   1049kB  1048kB           Free Space
    1      1049kB  10,7GB  10,7GB  primary  ext4
            10,7GB  16,1GB  5369MB           Free Space
    ```

5. Now resize the partition to the end (use the value from above):

    ```bash
    $ sudo parted /dev/sdb resizepart 1
    Warning: Partition /dev/sdb1 is being used. Are you sure you want to continue?
    Yes/No? Yes
    End?  [10,7GB]? 16,1GB                                                    
    Information: You may need to update /etc/fstab.
    ```

6.  If a logical volume is used, you can increase it now.

7.  Now as the löast step the filesystem can be increased:

    ```bash
    $ sudo resize2fs /dev/sdb1
    resize2fs 1.46.2 (28-Feb-2021)
    Dateisystem bei /dev/sdb1 ist auf /mnt1 eingehängt; Online-Größenänderung ist
    erforderlich
    old_desc_blocks = 2, new_desc_blocks = 2
    Das Dateisystem auf /dev/sdb1 is nun 3930408 (4k) Blöcke lang.
    ```

{!docs/assets/abbreviations.txt!}
