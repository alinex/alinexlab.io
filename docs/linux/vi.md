title: vi

# vi / Vim Editor

The vi editor is a basic editor which is available on nearly any Unix system within the core installation.
On the first look the editor with it's control keys looks cumbersome. But after learning how to use it you will find that all things are easy done with only a few key strokes.

The first thing to know is that it uses different modes. You start in the command mode and to edit text you should switch to the insert mode. The status bar on the bottom line shows if a special mode is active and will be used for special commands.

!!! note

    Vim is an advanced version of vi adding colorful syntax highlighting, unlimited undo and an optimized GUI. Often a shim is installed to link the `vi` call directly to Vim. That makes it a full replacement.

## Configuration

Vim can be configured system wide (globally) via the `/etc/vim/vimrc.local` or per user with `~/.vimrc`:

```text
set mouse=r
syntax on
```

Possibilities:

-   `set number` - to enable line numbers on the left
-   `syntax on` - to enable syntax highlighting
-   `colorscheme COLOR_SCHEME_NAME` - color schema can be one of: default, blue, darkblue, delek, desert, elford, evening, industry, koehler, morning, murphy, pablo, peachpuff, ron, shine, slate, torte, zellner
-   `set tabstop=4` - set the tab size
-   `set autoindent` - to indent while writing
-   `set expandtab` - will replace tabs with spaces on store
-   `set softtabstop=4` - will remove multiple spaces with ++backspace++
-   `set cursorline` - to highlight the cursor line
-   `set mouse=r` - to allow copy and paste using mouse selection

## General Keys

This mode is active after starting vi and regular keys are linked to special commands so you can't just start typing. The commands consist of one or two simple key strokes without a ++return++ after it.

| Keys                    | Action                                                                |
| ----------------------- | --------------------------------------------------------------------- |
| ++esc++                 | Switch to **command** mode.                                           |
| ++i++                   | Switch to **insert** mode before cursor.                              |
| ++shift+i++             | Switch to **insert** mode at beginning of current line.               |
| ++a++                   | Switch to **insert** mode after cursor.                               |
| ++shift+a++             | Switch to **insert** mode at the end of current line.                 |
| ++o++                   | Switch to **insert** mode as new line above cursor.                   |
| ++shift+o++             | Switch to **insert** mode as new line below cursor.                   |
| ++shift+r++             | Switch to **replace** mode in which the existing text is overwritten. |
| ++shift+z++ ++shift+z++ | **Save** file **and exit** vi (like `:wq`).                           |
| ++colon++               | Give a **execution** command in the bottom line.                      |

## Navigation

While in command mode a lot of commands help to easy go through the file.

| Keys                                  | Action                                                                                                                                              |
| ------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| ++left++ or ++h++                     | Move cursor position one character left.                                                                                                            |
| ++down++ or ++j++ or ++plus++         | Move cursor position one character down.                                                                                                            |
| ++up++ or ++k++ or ++minus++          | Move cursor position one character up.                                                                                                              |
| ++right++ or ++l++                    | Move cursor position one character right.                                                                                                           |
| ++home++ or ++0++ or ++circumflex++   | Move cursor to the start of the current line.                                                                                                       |
| ++end++ or ++dollar++                 | Move cursor to the end of the current line.                                                                                                         |
| ++w++                                 | Move forward one word (alphanumeric characters make up words).                                                                                      |
| ++shift+w++                           | Move forward one word (white space separates words).                                                                                                |
| ++e++                                 | Move forward one word to the end (alphanumeric characters make up words).                                                                           |
| ++shift+e++                           | Move forward one word to the end (white space separates words).                                                                                     |
| ++b++                                 | Move backward one word (alphanumeric characters make up words).                                                                                     |
| ++shift+b++                           | Move backward one word (white space separates words).                                                                                               |
| ++shift+g++                           | Go to the end of the file.                                                                                                                          |
| `<num>G` like ++4++ ++0++ ++shift+g++ | **Go to** a specific line (here line 40).                                                                                                           |
| `<num><dir>` like ++4++ ++right++     | Type a number and then a direction key to move multiple steps (here 4 times right).                                                                 |
| ++f++ `<char>`                        | Find the character corresponding to the next keystroke typed. Move the cursor to the next occurrence of that _character_ (on the current line only) |
| ++shift+f++ `<char>`                  | Same as ++f++ but movement is backwards.                                                                                                            |
| ++semicolon++                         | Repeat the last ++f++ or ++shift+f++ command.                                                                                                       |
| ++shift+h++                           | Move cursor to the top line of the screen, (as opposed to the top of the document which may not be the same place).                                 |
| ++shift+m++                           | Move cursor to the middle of the screen.                                                                                                            |
| ++shift+l++                           | Move cursor to the last line on the screen.                                                                                                         |
| ++percent++                           | Move cursor to the matching parenthesis, bracket or brace. Great for debugging programs.                                                            |
| ++parenthesis-left++                  | Move cursor to the beginning of the previous sentence (where a punctuation mark and two spaces define a sentence).                                  |
| ++parenthesis-right++                 | Move cursor to the beginning of the next sentence.                                                                                                  |
| ++brace-left++                        | Move cursor to the beginning of the current paragraph.                                                                                              |
| ++brace-right++                       | Move cursor to the beginning of the next paragraph.                                                                                                 |
| ++slash++ `<term>`                    | Finds the next occurrence of the term.                                                                                                              |
| ++question++ `<term>`                 | Finds the previous occurrence of the term.                                                                                                          |
| ++n++                                 | Repeats the last search command. Finds the next occurrence.                                                                                         |
| ++shift+n++                           | Repeats the last search command. Finds the previous occurrence.                                                                                     |
| ++plus++                              | Move cursor to start of next line.                                                                                                                  |
| ++minus++                             | Move cursor to start of previous line.                                                                                                              |
| ++ctrl+f++                            | Scroll forward one screen.                                                                                                                          |
| ++ctrl+b++                            | Scroll backward one screen.                                                                                                                         |
| ++ctrl+d++                            | Scroll forward one half screen (down).                                                                                                              |
| ++ctrl+u++                            | Scroll backward one half screen (up).                                                                                                               |
| ++z++ ++plus++                        | Move the current line to the top of the screen and scroll.                                                                                          |
| ++z++ ++period++                      | Move the current line to the center of the screen and scroll.                                                                                       |
| ++z++ ++minus++                       | Move the current line to the bottom of the screen and scroll.                                                                                       |
| ++shift+h++                           | Move to home—the top line on the screen.                                                                                                            |
| ++shift+m++                           | Move to the middle line on the screen.                                                                                                              |
| ++shift+l++                           | Move to the last line on the screen.                                                                                                                |

## Editing in Command Mode

| Keys                                 | Action                                                                                                  |
| ------------------------------------ | ------------------------------------------------------------------------------------------------------- |
| ++x++                                | **Remove** character under cursor.                                                                      |
| ++r++ `<char>`                       | **Replace** character under cursor with other character.                                                |
| ++c++ `<nav>`                        | **Change** the text from cursor position to the navigation position (remove and switch to insert mode). |
| ++c++ ++w++                          | Change word the next word.                                                                              |
| ++c++ ++2++ ++b++                    | Change back two words.                                                                                  |
| ++shift+c++ same as ++c++ ++dollar++ | Change the line from cursor to end of line.                                                             |
| ++c++ ++c++ or ++shift++ ++s++       | Change the whole current line.                                                                          |
| ++s++                                | Substitute character under cursor (remove and switch to insert mode).                                   |
| ++d++ `<nav>`                        | **Delete** the text from cursor position to the navigation position.                                    |
| ++d++ ++d++                          | Delete current line.                                                                                    |
| `<num>` ++d++ ++d++                  | Delete `num` lines starting at current line.                                                            |
| ++y++ `<nav>`                        | **Copy** the text from cursor position to the navigation position.                                      |
| ++p++                                | **Paste** line(s) you deleted (or copied) back into the file.                                           |
| ++u++                                | Undo last change.                                                                                       |
| ++shift+u++                          | Undo all changes on line.                                                                               |
| ++exclam++ `<command>`               | **Filter** text through a program.                                                                      |
| ++greater++ ++greater++              | Indent line.                                                                                            |
| ++greater++ `<num>` ++greater++      | Indent number of lines lines (starting from current).                                                   |
| ++less++ ++less++                    | Outdent line.                                                                                           |
| ++less++ `<num>` ++less++            | Outdent number of lines lines (starting from current).                                                  |
| ++period++                           | Repeat the last editing command.                                                                        |
| ++u++                                | Undo the last change.                                                                                   |
| ++shift+u++                          | Undo all changes in the current line.                                                                   |
| ++shift+j++                          | Join the current with the next line (replace newline with space).                                       |
| ++tilde++                            | Toggle case on cursor position.                                                                         |

## Execution Commands

Execution commands start with ++colon++ in the command mode and will be written in the bottom line:

| Command    | Action                                           |
| ---------- | ------------------------------------------------ |
| `:q`       | Quit vi but ask for saving if changes were made. |
| `:q!`      | Quit without saving.                             |
| `:wq`      | Write and quit vi.                               |
| `:e!`      | Wipe out all edits made in the current session.  |
| `:sh`      | Execute shell command.                           |
| `:r<file>` | Insert file after current line.                  |

{!docs/assets/abbreviations.txt!}
