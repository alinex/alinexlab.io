# Banner

The banner is used as information panel with possible buttons. It will take the whole width.

## Module Exports

- Default: [Banner](#banner_1)
- [Label](common.md#label)
- [Icon](common.md#icon)
- Banner: Type
- BannerComponentDev: Type
- CloseReason: Type

### Banner

Properties:

- `use` - pass actions to the dom element
- `class` - adding class names
- `component` - top level component
- `style` - add style settings
- `open` - current open state (default: false)
- `centered` - (default: false)
- `fixed` - (default: false)
- `mobileStacked` - (default: false)
- `content$class` -
- `textWrapper$class` -
- `graphic$class` -

Methods:

- `getElement()` - return its top level DOM element.
- `isOpen()` - check if drawer is opened
- `setOpen()` - set open state
- `layout()` -

Slots:

- `icon`
- `label`
- `actions`

## Examples

### Banner with two Buttons

```html
<script lang="ts">
  import Banner, { Icon, Label } from "@smui/banner";
</script>

<Banner
  bind:open
  bind:centered
  bind:mobileStacked
  on:SMUIBanner:closed="{handleBannerClosed}"
>
  <Icon slot="icon" class="material-icons">favorite</Icon>
  <label slot="label">This is a banner with an icon and some actions.</label>
  <svelte:fragment slot="actions">
    <button secondary>Secondary</button>
    <button>Primary</button>
  </svelte:fragment>
</Banner>
```

## More Info

Button: [Demo](https://sveltematerialui.com/demo/drawer/) - [Material Design](https://material.io/components/navigation-drawer) - [Material Code](https://github.com/material-components/material-components-web/tree/v11.0.0/packages/mdc-drawer)

{!docs/assets/abbreviations.txt!}
