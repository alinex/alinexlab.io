title: Migration

# Migration Management

Migration is the change of code and structure. There are different distinct levels of migration with increasing complexity, cost and risk:

1.  Simple **upgrade** migration involves the movement to a newer version in the same environment.
2.  Moving to a **new machine** with the same software version may bring some side effects.
3.  Change the **database** system to a newer version or other database engine.
4.  More complicated is the moving to a different **programming language**.
5.  Migrating to an entirely **new platform** or operating system is the most complex type of migration.

## Upgrade to newer version

Hopefully the software will support you there and a simple upgrade call will do all the work for you. Possible steps here are:

-   Stop the running system.
-   Detect the environment with it's settings.
-   Make a backup of the code.
-   Install new code, overwrite the old one.
-   Make a backup of the database.
-   Update database structure and update data.
-   Make further changes to the file systems like logs, service scripts, configuration.
-   Startup System again.

Often the software uses some migration helper which contains up/down scripts to step one or multiple steps through the version list. This is a concept which will be described further on in the next chapters.

!!! note

    On virtual machines it is a good practice to not update the live system but to clone it, update the clone and switch if everything works.

### Track current Version

To help migrating it is essential to know what the current setup is. So the software or you know what is to be done to get up-to-date. Therefore each migration should have a version which is alphabetically linear to know the order. So at first a name scheme is needed. Therefore best use a [semantic version](https://semver.org/):

    v1.4.0-alpha
    v1.4.0-alpha1
    v1.4.0-patch
    v1.4.0-patch2
    v1.4.0-patch9
    v1.4.0-patch10
    v1.4.0
    v1.5.0-alpha
    v1.5.0-alpha1
    v1.5.0-alpha2
    v1.5.0
    v1.5.1
    v1.5.2

The changes may be stored in the filesystem in a log like:

    2020-11-16 14:36 UP v1.5.0
    2020-11-16 15:25 UP v1.5.1
    2020-11-17 14:36 UP v1.5.2

Or it may be stored in the database:

```sql
create table myapp_migration (
    `appliedAt` timestamp not null default CURRENT_TIMESTAMP,
    `version` varchar(256) not null,
    `description` varchar(256),
    primary key (`version`)
) ENGINE=InnoDB;
```

## Changing the Host

If the environment is the same (maybe a newer version of some components) the software should generally work again. But it depends and you have to check and try before you know for sure. Your steps should be:

-   Check if the software is up-to-date, if not update the software first.
-   Check if the newer environment is supported, too.
-   Check connectivity of newer machine to all used databases and services (firewall).
-   Install software on newer system with all needed helpers.
-   Copy data from the old installation.
-   Test the new system if it works correctly, if not fix it.
-   Stop the current running system.
-   Copy the data again, to be up-to.date.
-   Start the newer system and test it again.
-   Now you may switch the nameserver/loadbalancer record to point to the new system and make it active.
-   On problems you may switch bag.
-   After some days the old system can be removed.

This is only a general plan. Depending on the system you have to move more complexity may evolve.

## Switch database

Between databases of the same type like newer version or switch from mysql to postgres it may be possible to:

-   Dump into SQL.
-   Restore complete data structure.

But maybe it is a better idea to setup a new initial database with the software help and transfer only the data afterwards.

## Other programming language

You want to change the programming language? That is a big project but you can completely plan it. But before you begin take time to make a clear decision, is it really necessary? What are the big advantages?

Have a look at the architecture maybe it can also be done in steps, one module at a time. Then keep a small team which will maintain the older code while the other coders are working building the new system. This is also a perfect time to check if some bigger changes to the software are made by the way. Is everything in the software needed and used? Is something missing? Did you complain about bad design decisions in the last years? If any of this is true, that's the best time to change.

While developing got on, don't haste and go live to early. You should wait till the new software is at least as feature ready and stable as the old one to not lose some of your users. Take your time for excessive Testing with UnitTest, functional testing, security checks and make a release candidate to get experience from some key users if possible.

## Entirely new platform

That may be a hard way with lots of try, error and bug fixing in the software, too. Keep an open time frame for this because with the needed changes in software to work again like you need it it may also need a lot of development.

{!docs/assets/abbreviations.txt!}
