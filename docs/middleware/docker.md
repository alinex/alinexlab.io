# Docker & Co

With Docker, you can package your application along with all its dependencies into a container. Containers allow your applications to be deployed easily and uniformly.

It consists of:

- **Registries** - collection of images
- **Images** - package to be installed
- **Build** - to create new images
- **Containers** - runable instance of image (like small virtual machine)
- **Runtime** - to execute the container within the container manager
- **Orchestration** - for automatic arrangement in a farm
- **Image Optimization** - some tools help to inspect and optimize images

For most of this steps you can use docker which has to be licensed for bigger companies or some alternatives.

- Build alternative ist `Buildah` or the `Buildkit` docker integrated engine and `Kaniko` which is specialized to build within a container
- Container management can be done with `docker` or `Podman`
- Runtime: `runc` or `crun`
- Analyzation and optimization of images can be done using `Skopeo` and `Dive`

Some of these alternatives are described here, too.

## Installation

!!! attention

    As docker will search for a possible netmask to use, activate all VPN connections before installing. If not see problems below.

=== "Debian/Ubuntu"

    ```bash
    # install prerequisites
    sudo apt-get update
    sudo apt-get install ca-certificates curl gnupg lsb-release
    # remove old version
    sudo apt remove -y docker docker-engine docker.io containerd runc
    # add GPG key and repositories for docker
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    # install packages
    sudo apt update
    sudo apt install -y docker-ce docker-ce-cli
    # configuration
    sudo usermod -a -G docker operator # allow yiour user to execute docker commands
    # start and enable docker service
    sudo systemctl start docker
    sudo systemctl enable docker.service
    ```

=== "Arch Linux"

    ```bash
    # install docker
    sudo pacman -Sy docker
    # configuration
    sudo gpasswd -a operator docker # allow operator to use docker without sudo
    # start and enable service
    sudo systemctl start docker.service
    sudo systemctl enable docker.service
    ```

For production environment you may also use:

- containerd.io - for managing the docker containers
- docker-compose - for multi-docker environments
- docker-ctop - as CLI monitoring

=== "Debian/Ubuntu"

    ```bash
    # add GPG key and repositories for docker-ctop
    wget -qO - https://azlux.fr/repo.gpg.key | sudo apt-key add -
    echo "deb http://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
    # install packages
    sudo apt update
    sudo apt install -y containerd.io docker-compose docker-ctop
    # start and enable docker service
    sudo systemctl start containerd.service
    sudo systemctl enable containerd.service
    ```

=== "Arch Linux"

    ```bash
    # install docker
    sudo pacman -Sy docker-compose containerd ctop
    # start and enable docker service
    sudo systemctl start containerd.service
    sudo systemctl enable containerd.service
    ```

### Network Problems

I had a big Problem with the network on the docker host. It looks like docker will setup one just at installation depending on what is unused. But if you later start a VPN network it is used in prior to the VPN.

I checked a lot of solutions like changing the `daemon.json`... Neither worked.

Only solution was to remove docker completely, remove all residing files using and reinstall docker:

=== "Arch Linux"

```bash
# uninstall
sudo pacman -Rcs docker
# search and remove docker files
sudo find / -name docker\*
sudo rm -r /var/lib/docker /etc/docker /run/docker*
sudo find / -name docker\*
# install docker again but with all VPN connections active
sudo pacman -Syu docker
```

## Use Images

To experiment you can search and download images:

```bash
# search for image
docker search <image-name>
# download only image
docker pull <image>
```

!!! info

    You can also search for the package in the browser https://hub.docker.com

If you run an image it will be loaded from registry if not locally existent:

??? example "docker run hello-world"

        Unable to find image 'hello-world:latest' locally
        latest: Pulling from library/hello-world
        2db29710123e: Pull complete
        Digest: sha256:cc15c5b292d8525effc0f89cb299f1804f3a725c8d05e158653a563f15e4f685
        Status: Downloaded newer image for hello-world:latest

        Hello from Docker!
        This message shows that your installation appears to be working correctly.
        ....

Furthermore you can start and stop it:

```bash
docker run <container>
docker stop <container>
```

And a container can be removed again:

```bash
docker rm <container>
```

At last you can call commands like a bash shell within the container:

```bash
docker exec -it <container> bash
```

If you start a container without a name it will auto generate one for you. To better identify the container you may provide one using the `--name` argument:

```bash
docker run <image> --name <containername>
```

### Start as Daemon

With the `-d` argument the docker image will start as daemon in the background:

```bash
# start as daemon
container=$(docker run -p 3000:3000 -d alinex/svelte-app:test)
# show logs
docker log $container
# stop daemon
docker stop $container
```

## Create Images using Dockerfile

The `Dockerfile` defines how to build docker images. Find all the details at [docker.com](https://docs.docker.com/engine/reference/builder).

Docker images can be created in different ways:

1. from scratch
2. based on another image
3. out of a container

!!! urgent
  
    Make the image as small as possible.

Scratch is a minimalistic, empty image to start with if you have all to put into it locally ready to use. If you need to install and build something first better start with an official repo image. 
For security reasons you should not use some images from authors you not know and only from trusted organizations. So better build your images by yourself to know what is in them.

For all the details see https://docs.docker.com/develop.

### Official images to use

If you are not working from scratch use some official repos as base. Best is to specify at least the major version and don't use the latest tag which may give you some probblems later:

- [alpine](https://hub.docker.com/_/alpine)

### Commands

| Command                          | Description                                                                |
| -------------------------------- | -------------------------------------------------------------------------- |
| `FROM scratch`                   | Completely build on your own with only one binary or more.                 |
| `FROM <imagee>`                  | Use another image as base.                                                 |
| `ARG <name> <data>`              | Set environment variable from argumen, only for build.                     |
| `ENV <name> <data>`              | Set environment variable for build and image.                              |
| `LABEL <name>=<data>`            | Add meta information (see below).                                          |
| `WORKDIR`                        | Specify base folder for `RUN`, `COPY`, `ADD` and `ENTTRYPOINT`.            |
| `COPY <local path> <dir>`        | Copy local paths or files into the image.                                  |
| `RUN <command>`                  | Execute a command (makes new layer).                                       |
| `ADD`                            | Copy files and directories into image with extract and remote URL support. |
| `ENTRYPOINT`                     | Always start in container.                                                 |
| `CMD [<command>, <argument>...]` | Command to run as default in image.                                        |
| `VOLUME`                         | Specify mount points to be used.                                           |
| `EXPOSE`                         | Specify used ports.                                                        |

### Meta Information

Labels can be added with any name. A common approach is to use them according to [label-schema.org standard](http://label-schema.org/rc1/):

```Dockerfile
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name=$PATH
LABEL org.label-schema.description="Used to build docker images from Dockerfile"
LABEL org.label-schema.maintainer="Alexander Schilling <info@alinex.de>"
#LABEL org.label-schema.vendor="company"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.url="https://gitlab.com/$PATH/-/blob/main/README.md"
LABEL org.label-schema.vcs-url="https://gitlab.com/$PATH"
#LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.docker.cmd="docker run -v \$(pwd):/data $NAME"
```

### Create from Image

Images are created using instructions from a `Dockerfile`.

    FROM alpine:3.14
    COPY /hello /
    RUN cat /hello

Then build it from within the directory of the `Dockerfile`:

```bash
docker build -t helloapp:v1 .
```

Or without a dockerfile, create it directly

```bash
docker build -t helloapp:v1 -f- . -<<EOF
FROM alpine:3.14
COPY /hello /
RUN cat /hello
CMD ["cat", "/hello"]
EOF
```

The `-f- .` is needed here to specify root for `COPY` and `ADD`.

!!! note

    Use a `.dockerignore` file to exclude files for docker.

### Cleanup Image

To make a small image after complex creation/build process, you often can remove unnecessary parts by making a new image with only the results. At the end of the `Dockerfile` add:

    # cleanup to single layer image
    FROM scratch
    COPY --from=build /bin/project /bin/project
    ENTRYPOINT ["/bin/project"]
    CMD ["--help"]

### Create Image from Container

```bash
docker commit <container> <image>
```

### BuildKit

This is an improved build engine which will work often faster and is integrated into docker.

To run it set the environment variable:

```bash
DOCKER_BUILDKIT=1 docker build ...
```

Or set it generally in

=== "/etc/docker/daemon.json"

    ```json
    { "features": { "buildkit": true } }
    ```

### Kaniko

As Docker in Docker is not allowed at gitlab.com I use [kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html) here.

Therefore the following variables are defined

| Key                  | Value                       | Protected | Masked |
| -------------------- | --------------------------- | --------- | ------ |
| CI_REGISTRY          | https://index.docker.io/v1/ | no        | no     |
| CI_REGISTRY_IMAGE    | docker.io/alinex/mkdocs     | no        | no     |
| CI_REGISTRY_PASSWORD | xxxxxxxxxxxxxx              | no        | yes    |
| CI_REGISTRY_USER     | alinex                      | no        | no     |

The following configuration will build the docker file and upload it to https://hub.docker.com:

```yaml
build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:latest"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
  rules:
    - if: $CI_COMMIT_TAG
```

## Analysis

An interactive tool for this is [dive](https://github.com/wagoodman/dive) which let's you analyze an image in deepth and find optimization points.

Details on any docker image or container can be gathered using `docker inspect <image>`. It will give a JSON output:

```json
[
  {
    "Id": "sha256:ea439aa9864d630d070ebcc92eec32af5b3705868442758925338299b4c918f7",
    "RepoTags": ["alinex/mkdocs:latest"],
    "RepoDigests": [
      "alinex/mkdocs@sha256:663a2e1961be887891d9f0297d15bc606586f76555fb3c4ed3eb1937ca5aff5b"
    ],
    "Parent": "",
    "Comment": "",
    "Created": "2021-11-22T21:40:27.016147331Z",
    "Container": "4b2e142da7e297b4d49ed1c46023e35116cbec0cf1d9c83e7ea407f966efd2e0",
    "ContainerConfig": {
      "Hostname": "",
      "Domainname": "",
      "User": "",
      "AttachStdin": false,
      "AttachStdout": false,
      "AttachStderr": false,
      "Tty": false,
      "OpenStdin": false,
      "StdinOnce": false,
      "Env": null,
      "Cmd": null,
      "Image": "",
      "Volumes": null,
      "WorkingDir": "",
      "Entrypoint": null,
      "OnBuild": null,
      "Labels": null
    },
    "DockerVersion": "20.10.7",
    "Author": "",
    "Config": {
      "Hostname": "",
      "Domainname": "",
      "User": "",
      "AttachStdin": false,
      "AttachStdout": false,
      "AttachStderr": false,
      "Tty": false,
      "OpenStdin": false,
      "StdinOnce": false,
      "Env": [
        "PATH=/usr/local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
        "LANG=C.UTF-8",
        "GPG_KEY=A035C8C19219BA821ECEA86B64E628F8D684696D",
        "PYTHON_VERSION=3.10.0",
        "PYTHON_PIP_VERSION=21.2.4",
        "PYTHON_SETUPTOOLS_VERSION=57.5.0",
        "PYTHON_GET_PIP_URL=https://github.com/pypa/get-pip/raw/3cb8888cc2869620f57d5d2da64da38f516078c7/public/get-pip.py",
        "PYTHON_GET_PIP_SHA256=c518250e91a70d7b20cceb15272209a4ded2a0c263ae5776f129e0d9b5674309"
      ],
      "Cmd": ["/run.sh"],
      "Image": "sha256:db5b48969e1eee6ea559e5ac060430cb23e503aa1d0d2f88cbba6efccd49466c",
      "Volumes": null,
      "WorkingDir": "",
      "Entrypoint": null,
      "OnBuild": null,
      "Labels": null
    },
    "Architecture": "amd64",
    "Os": "linux",
    "Size": 686492251,
    "VirtualSize": 686492251,
    "GraphDriver": {
      "Data": {
        "LowerDir": "/var/lib/docker/overlay2/ce4f5c7028cc92d063503abb6ad6e7a74e6eae95d5e8540e549b4f7d78fee664/diff:/var/lib/docker/overlay2/8e2179dc90b21cf4a5fcc1ade27533dd0e818cb84461483ff3dd09afbb00180f/diff:/var/lib/docker/overlay2/2a392c4c1b775236ff2dd2964d6cdc4d29e6ef6c901a68be4c5cb803f11edb51/diff:/var/lib/docker/overlay2/24c485f245ac4e8f263f25c36def36eaa99f85c5a7de5913bfd87c667e095b40/diff:/var/lib/docker/overlay2/325b5db54641fb49948ce620ad562cf8ac84ee7d4d4680a19a8b1a0ae406ff8d/diff:/var/lib/docker/overlay2/0b61b0876bfb90e52c741e93693538112dc079827f76f7223b2c9b17e3be8fed/diff:/var/lib/docker/overlay2/877b13d0e8f0d8d26a1b8be0d6de091d4e4e18c1eb5d2219415df1ac44f47be6/diff:/var/lib/docker/overlay2/226c2625a711595cc27be89e36bc5f1d0c94779ec3ffbe7924347a485f25fc45/diff:/var/lib/docker/overlay2/096914913e61b643b52488880c27d2ae083b05d51e9ceae26dce3c3d90305959/diff:/var/lib/docker/overlay2/a89943052fecb67759d36cb8d26c5921a071d16f330835820886f009d15ab9e2/diff:/var/lib/docker/overlay2/db49592c50209326b4661058b2d9a175562b4fcd1f442656f2bc00dab7a2475a/diff:/var/lib/docker/overlay2/2ef3734a419a5b1022b436ba1ec483fc09125f7c6a3ddd163c51d9d9947efc96/diff:/var/lib/docker/overlay2/2ba750be99dfcd7fb59f7c510b68504a27b890f225c8785893d9e050fb6fefb3/diff:/var/lib/docker/overlay2/6f06bf8037b540741c04e3bfb52929ae06a87c56e63ccc820b2a066254f14ef5/diff:/var/lib/docker/overlay2/5f0faf539f573597e8374f0faae21ff9f36249387ffc4de85fd4dd25c63f00d3/diff:/var/lib/docker/overlay2/d757814c73034df17fb151c8ef6bfc2e3c3a23c5f77fa15399a3570d135737bd/diff:/var/lib/docker/overlay2/239d65babe70b1f79c5dd234cfe928ebc894862e045aba4cc101650eb3778c08/diff",
        "MergedDir": "/var/lib/docker/overlay2/40a13812bef2ebce2eff25f36f530158d3bc1e7dd68efac1240fefa8fedf760c/merged",
        "UpperDir": "/var/lib/docker/overlay2/40a13812bef2ebce2eff25f36f530158d3bc1e7dd68efac1240fefa8fedf760c/diff",
        "WorkDir": "/var/lib/docker/overlay2/40a13812bef2ebce2eff25f36f530158d3bc1e7dd68efac1240fefa8fedf760c/work"
      },
      "Name": "overlay2"
    },
    "RootFS": {
      "Type": "layers",
      "Layers": [
        "sha256:e1bbcf243d0e7387fbfe5116a485426f90d3ddeb0b1738dca4e3502b6743b325",
        "sha256:7944c75516ae5cec5fac2b3e2cfc745c87a0b4d1cc95a7ba806a7a63f6401ca6",
        "sha256:775d27396430257ec15d711f078e70f7899a18647e7e61d92b482978ac4b5916",
        "sha256:70c19fb3395a41586d49f632b22a5336d8ef60973363ca63e4f3228f02295d62",
        "sha256:834714e112d6855846c523ba70b3b3622c646601b53fd05336664b7499abe0ea",
        "sha256:a157178a0bc679b673bf240700a73381eb1662e629c42ce2aec9ddff828316dc",
        "sha256:ab0cd85f69e8dc77f4676d8fcd7e2a5024b5ca632ebcba89282f8f0702e70818",
        "sha256:ed4500c19ac6d943208beadb38a6bdc27fe28bae22ccf90017bbc5f543d7c621",
        "sha256:5c16534c9590eeeeda5c9e0a342dff9f9120318cd4f6c42654d8aacdc0dad12f",
        "sha256:ce003603e5ce408fae2a324e8974d4de3cb328ec9a8542e63f71a25d34a19b86",
        "sha256:ddabae7b1028b792ad65a61f2e5c8ee73cae87e3b7544554c6c3fc4ddb5a8dad",
        "sha256:802e78e8565f1de5fbdf301be8fba30fd9f229064bfe06ce41b331c1f3bb1119",
        "sha256:d34c37dee1c2999248bc57ed8d3e92045e19d5117fc940030c93ce9d212cc0f1",
        "sha256:eb166e771f1a653f091ba9361edd4b3aae2d26135886ca46fd5681f543790e21",
        "sha256:fa1ea3ecbaad4512c723cb8051cebab315a6da3ab196d930d546b577330bed88",
        "sha256:3d8a24e72cf0aa752d8ea9f758915257748859315ab1adc227d8b20febe2a1e3",
        "sha256:5d2f72571bfa2be3174c51380a5f154d155884181d6c30f2c3a91d118f2a8cfe",
        "sha256:81bde528645f45b78c2ab121d8d46e91b419b3fb81bde6755dd1dbc761faf2f7"
      ]
    },
    "Metadata": {
      "LastTagTime": "0001-01-01T00:00:00Z"
    }
  }
]
```

The same can be done using [Skopeo](https://github.com/containers/skopeo) also without downloading images.

## OLD OLD OLD
### Access Host Filesystem

You can use Docker volumes to share files between a host system and the Docker container. This can be used to store application data outside of the container, to make upgrades easier.

Therefore make directory for the image and share it on start of the docker:

```bash
mkdir /opt/<container>
docker run -d -P --name <container> -v /opt/<container>:/data <image>
```

All files within `/opt/<container>` will be availble under `/data` within the docker container.

## Docker based Hosting

Hosting of Docker applications is possible on different solutions:

- Public Cloud like AWS, Google Cloud, Azure and more
- Own private cloud using orchestration like Kubernetes
- Own hosts for smaller docker applications

### Own Host

If you have to host only some smaller applications you can use your own servers. Therefore best use a linux host with 20GB root partition and an additional data partition mounted on `/data` or `/mnt1`. Then link the `/opt` and `/var/lib/docker` folders to it:

```bash
sudo mv /opt /data
sudo ln -s /data/opt /
sudo mv /var/lib/docker /data || sudo mkdir /data/docker
sudo ln -s /data/docker /var/lib
```

With such a setup you can put the application's persistent data within `/opt` and the docker storage at it's normal space in `/var/lib/docker`. The separate root partition helps to keep the system healthy also if the docker storage is getting full.

Also you should add some automatic cleanups and remove old images and containers to free space using a cron job:

    # m h dom mon dow command
    0 6 * * MON docker system prune -f >/dev/null 2>&1

If you have to move an application to another host, you should stop it, transform their `/opt/xxx` files to the new host and run the docker container there.

## Operations

### Analysis

#### Installed Containers

If `ctop` is installed like shown above you may always use this. as easy way to show and control containers.
It shows real-time metrics for multiple containers as well as a single container view for inspecting a specific container.

Select a container from the list to switch to single view or to show logs, start/stop or remove the container.

But you can also directly analyze docker using the CLI:

??? example "sudo docker system df -v"

        Images space usage:

        REPOSITORY                TAG                 IMAGE ID            CREATED             SIZE                SHARED SIZE         UNIQUE SIZE         CONTAINERS
        selenoid/vnc_chrome       94.0                d99256390f8b        3 days ago          1.127GB             742.7MB             384.2MB             0
        aerokube/selenoid-ui      latest              db3188cc5703        9 days ago          17.53MB             0B                  17.53MB             0
        selenoid/vnc_opera        79.0                6f7f9ee50e3e        2 weeks ago         1.059GB             742.7MB             316.2MB             0
        selenoid/vnc_firefox      92.0                2155aa85cb7e        3 weeks ago         1.102GB             742.7MB             359MB               0
        selenoid/vnc_chrome       93.0                947d694b017f        3 weeks ago         1.088GB             742.7MB             345.3MB             2
        selenoid/vnc_opera        78.0                75bf9808d31d        3 weeks ago         1.057GB             741.9MB             314.8MB             0
        aerokube/selenoid         1.10.5              ab24d9fc0486        3 weeks ago         16.49MB             0B                  16.49MB             0
        selenoid/vnc_firefox      91.0                917ac7d174d0        6 weeks ago         1.1GB               741.9MB             358.4MB             0
        selenoid/vnc_chrome       92.0                1af797eddcac        6 weeks ago         1.082GB             741.9MB             339.9MB             0
        selenoid/video-recorder   latest-release      654b710209f9        9 months ago        37.56MB             0B                  37.56MB             0
        aerokube/selenoid         1.9.1               531cd150ccb6        2 years ago         13.19MB             0B                  13.19MB             0

        Containers space usage:

        CONTAINER ID        IMAGE                      COMMAND             LOCAL VOLUMES       SIZE                CREATED             STATUS              NAMES
        c7dd11353588        selenoid/vnc_chrome:93.0   "/entrypoint.sh"    0                   1.88MB              20 hours ago        Up 20 hours         nice_wright
        5dc187fcd7b7        selenoid/vnc_chrome:93.0   "/entrypoint.sh"    0                   1.88MB              2 days ago          Up 2 days           focused_lamport

        Local Volumes space usage:

        VOLUME NAME         LINKS               SIZE

        Build cache usage: 0B

        CACHE ID            CACHE TYPE          SIZE                CREATED             LAST USED           USAGE               SHARED

??? example "sudo docker ps -as"

        CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                    PORTS               NAMES               SIZE
        c70eb9887e62        hello-world         "/hello"            9 months ago        Exited (0) 9 months ago                       kind_mendeleev      0B (virtual 13.3kB)
        313dd9a3ce8e        hello-world         "/hello"            9 months ago        Exited (0) 9 months ago                       sharp_mclean        0B (virtual 13.3kB)
        72df3671cbe8        hello-world         "/hello"            9 months ago        Exited (0) 9 months ago                       eager_poincare      0B (virtual 13.3kB)
        f3e7c0cccb60        hello-world         "/hello"            9 months ago        Exited (0) 9 months ago                       jovial_tharp        0B (virtual 13.3kB)

??? example "sudo docker container stats -a --no-stream"

        CONTAINER ID        NAME                CPU %               MEM USAGE / LIMIT     MEM %               NET I/O             BLOCK I/O           PIDS
        c7dd11353588        nice_wright         0.04%               100.4MiB / 1.948GiB   5.04%               5.99MB / 325MB      0B / 877kB          29
        5dc187fcd7b7        focused_lamport     0.03%               94.05MiB / 1.948GiB   4.72%               5.97MB / 342MB      389kB / 827kB       29

#### Manage Single Container

??? example "sudo docker inspect <container id>"

    ```json
    [
        {
            "Id": "c7dd11353588538d44c4defb7c361da628da3b1f9f9f9c7d3fcffd7a2bc3e87f",
            "Created": "2021-09-30T11:37:17.974605045Z",
            "Path": "/entrypoint.sh",
            "Args": [],
            "State": {
                "Status": "running",
                "Running": true,
                "Paused": false,
                "Restarting": false,
                "OOMKilled": false,
                "Dead": false,
                "Pid": 19582,
                "ExitCode": 0,
                "Error": "",
                "StartedAt": "2021-09-30T11:37:18.239167206Z",
                "FinishedAt": "0001-01-01T00:00:00Z"
            },
            "Image": "sha256:947d694b017f69b3499fee61246f1c95d93c32750eb04f29a75b54228142edc6",
            "ResolvConfPath": "/var/lib/docker/containers/c7dd11353588538d44c4defb7c361da628da3b1f9f9f9c7d3fcffd7a2bc3e87f/resolv.conf",
            "HostnamePath": "/var/lib/docker/containers/c7dd11353588538d44c4defb7c361da628da3b1f9f9f9c7d3fcffd7a2bc3e87f/hostname",
            "HostsPath": "/var/lib/docker/containers/c7dd11353588538d44c4defb7c361da628da3b1f9f9f9c7d3fcffd7a2bc3e87f/hosts",
            "LogPath": "/var/lib/docker/containers/c7dd11353588538d44c4defb7c361da628da3b1f9f9f9c7d3fcffd7a2bc3e87f/c7dd11353588538d44c4defb7c361da628da3b1f9f9f9c7d3fcffd7a2bc3e87f-json.log",
            "Name": "/nice_wright",
            "RestartCount": 0,
            "Driver": "overlay2",
            "Platform": "linux",
            "MountLabel": "",
            "ProcessLabel": "",
            "AppArmorProfile": "unconfined",
            "ExecIDs": null,
            "HostConfig": {
                "Binds": null,
                "ContainerIDFile": "",
                "LogConfig": {
                    "Type": "json-file",
                    "Config": {}
                },
                "NetworkMode": "default",
                "PortBindings": {},
                "RestartPolicy": {
                    "Name": "",
                    "MaximumRetryCount": 0
                },
                "AutoRemove": true,
                "VolumeDriver": "",
                "VolumesFrom": null,
                "CapAdd": null,
                "CapDrop": null,
                "Capabilities": null,
                "Dns": null,
                "DnsOptions": null,
                "DnsSearch": null,
                "ExtraHosts": null,
                "GroupAdd": null,
                "IpcMode": "private",
                "Cgroup": "",
                "Links": null,
                "OomScoreAdj": 0,
                "PidMode": "",
                "Privileged": true,
                "PublishAllPorts": false,
                "ReadonlyRootfs": false,
                "SecurityOpt": [
                    "label=disable"
                ],
                "UTSMode": "",
                "UsernsMode": "",
                "ShmSize": 268435456,
                "Runtime": "runc",
                "ConsoleSize": [
                    0,
                    0
                ],
                "Isolation": "",
                "CpuShares": 0,
                "Memory": 0,
                "NanoCpus": 0,
                "CgroupParent": "",
                "BlkioWeight": 0,
                "BlkioWeightDevice": null,
                "BlkioDeviceReadBps": null,
                "BlkioDeviceWriteBps": null,
                "BlkioDeviceReadIOps": null,
                "BlkioDeviceWriteIOps": null,
                "CpuPeriod": 0,
                "CpuQuota": 0,
                "CpuRealtimePeriod": 0,
                "CpuRealtimeRuntime": 0,
                "CpusetCpus": "",
                "CpusetMems": "",
                "Devices": null,
                "DeviceCgroupRules": null,
                "DeviceRequests": null,
                "KernelMemory": 0,
                "KernelMemoryTCP": 0,
                "MemoryReservation": 0,
                "MemorySwap": 0,
                "MemorySwappiness": null,
                "OomKillDisable": false,
                "PidsLimit": null,
                "Ulimits": null,
                "CpuCount": 0,
                "CpuPercent": 0,
                "IOMaximumIOps": 0,
                "IOMaximumBandwidth": 0,
                "MaskedPaths": null,
                "ReadonlyPaths": null
            },
            "GraphDriver": {
                "Data": {
                    "LowerDir": "/var/lib/docker/overlay2/756feedff60387cb1fd2f062c6cd84bea1ec2e09c4eef54cf09ce008b70116a1-init/diff:/var/lib/docker/overlay2/f357e4570949334c6c03e9d5fc13aa838bc790f61608573231bda57c20769643/diff:/var/lib/docker/overlay2/5fe84c28e48428251f21cb74cb11fc3b38ab05739a66cf495185db69049189f8/diff:/var/lib/docker/overlay2/2bb49f1f6783bdcbc5f7558720c6c967fac248d21a2394f46a159fc949408a9a/diff:/var/lib/docker/overlay2/9c8056a76ed9811e3b749508b930fef8238c768676f5c2d9f9686e27abe0b794/diff:/var/lib/docker/overlay2/08e04553306e17a989bcccf3fa8d04a6a572053ef383c3540e1a09f8b5591799/diff:/var/lib/docker/overlay2/fdb6ee2a232edf3d36fdff6e8978340bb792e4ac8d7aed357f4452e1ace136cd/diff:/var/lib/docker/overlay2/455bed34ecebe6b9fedb8007da174113cd2d861694a673375d6e73ea18a4d35b/diff:/var/lib/docker/overlay2/aa1758c864224bce5b6d668daff509543d933cc2f1cc24e6e16ebb8e1c3d63d8/diff:/var/lib/docker/overlay2/2adee72abf2e57c05e547ee0f3027fa313af443d6da0683593dec4a55a710ae7/diff:/var/lib/docker/overlay2/b3049a2406dd277ad92bee9aa30be8fe5e742e61d89c1c502a64632cce4ebfef/diff:/var/lib/docker/overlay2/90c0c781213aa515acf7c94fb5e2cdfbd671899a5b7f10f13a60baab5882cd31/diff:/var/lib/docker/overlay2/a83e24531a38134e9199e15e927fc5907d57c5641c43848f62abac1c21502bfc/diff",
                    "MergedDir": "/var/lib/docker/overlay2/756feedff60387cb1fd2f062c6cd84bea1ec2e09c4eef54cf09ce008b70116a1/merged",
                    "UpperDir": "/var/lib/docker/overlay2/756feedff60387cb1fd2f062c6cd84bea1ec2e09c4eef54cf09ce008b70116a1/diff",
                    "WorkDir": "/var/lib/docker/overlay2/756feedff60387cb1fd2f062c6cd84bea1ec2e09c4eef54cf09ce008b70116a1/work"
                },
                "Name": "overlay2"
            },
            "Mounts": [],
            "Config": {
                "Hostname": "localhost",
                "Domainname": "",
                "User": "selenium",
                "AttachStdin": false,
                "AttachStdout": false,
                "AttachStderr": false,
                "ExposedPorts": {
                    "4444/tcp": {},
                    "5900/tcp": {},
                    "7070/tcp": {},
                    "8080/tcp": {},
                    "9090/tcp": {}
                },
                "Tty": false,
                "OpenStdin": false,
                "StdinOnce": false,
                "Env": [
                    "TZ=UTC",
                    "SCREEN_RESOLUTION=1920x1080x24",
                    "ENABLE_VNC=true",
                    "ENABLE_VIDEO=true",
                    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                    "DBUS_SESSION_BUS_ADDRESS=/dev/null"
                ],
                "Cmd": null,
                "Image": "selenoid/vnc_chrome:93.0",
                "Volumes": null,
                "WorkingDir": "",
                "Entrypoint": [
                    "/entrypoint.sh"
                ],
                "OnBuild": null,
                "Labels": {
                    "browser": "google-chrome-stable:93.0.4577.63-1",
                    "driver": "chromedriver:93.0.4577.63"
                }
            },
            "NetworkSettings": {
                "Bridge": "",
                "SandboxID": "6df536f2cea9a374323288a4860c7053d653e7e5507b8f943f54deb951014d23",
                "HairpinMode": false,
                "LinkLocalIPv6Address": "",
                "LinkLocalIPv6PrefixLen": 0,
                "Ports": {
                    "4444/tcp": null,
                    "5900/tcp": null,
                    "7070/tcp": null,
                    "8080/tcp": null,
                    "9090/tcp": null
                },
                "SandboxKey": "/var/run/docker/netns/6df536f2cea9",
                "SecondaryIPAddresses": null,
                "SecondaryIPv6Addresses": null,
                "EndpointID": "65acd09f6e778ef35138cea4c3c13937a619acfb61ac9031b15a1269b1eb761c",
                "Gateway": "172.18.0.1",
                "GlobalIPv6Address": "",
                "GlobalIPv6PrefixLen": 0,
                "IPAddress": "172.18.0.4",
                "IPPrefixLen": 16,
                "IPv6Gateway": "",
                "MacAddress": "02:42:ac:12:00:04",
                "Networks": {
                    "bridge": {
                        "IPAMConfig": null,
                        "Links": null,
                        "Aliases": null,
                        "NetworkID": "8e2841bb94b5e781aa98d4656abdec60a576a8224353571fdc798f31efc6d59d",
                        "EndpointID": "65acd09f6e778ef35138cea4c3c13937a619acfb61ac9031b15a1269b1eb761c",
                        "Gateway": "172.18.0.1",
                        "IPAddress": "172.18.0.4",
                        "IPPrefixLen": 16,
                        "IPv6Gateway": "",
                        "GlobalIPv6Address": "",
                        "GlobalIPv6PrefixLen": 0,
                        "MacAddress": "02:42:ac:12:00:04",
                        "DriverOpts": null
                    }
                }
            }
        }
    ]
    ```

??? example "sudo docker container top <container id>"

        UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
        4096                19582               19564               0                   Sep30               ?                   00:00:00            /bin/bash /entrypoint.sh
        4096                19612               19582               0                   Sep30               ?                   00:00:00            /usr/bin/fileserver
        4096                19613               19582               0                   Sep30               ?                   00:00:00            /usr/bin/devtools
        4096                19614               19582               0                   Sep30               ?                   00:00:00            /usr/bin/xseld
        4096                19653               19582               0                   Sep30               ?                   00:00:00            pulseaudio --start --exit-idle-time=-1
        4096                19687               19582               0                   Sep30               ?                   00:00:00            /bin/sh /usr/bin/xvfb-run -l -n 99 -s -ac -screen 0 1920x1080x24 -noreset -listen tcp /usr/bin/fluxbox -display :99 -log /dev/null
        4096                19707               19687               0                   Sep30               ?                   00:00:00            Xvfb :99 -ac -screen 0 1920x1080x24 -noreset -listen tcp -auth /tmp/xvfb-run.5CT3ew/Xauthority
        4096                19716               19687               0                   Sep30               ?                   00:00:00            /usr/bin/fluxbox -display :99 -log /dev/null
        4096                19733               19582               0                   Sep30               ?                   00:00:00            x11vnc -display :99 -passwd selenoid -shared -forever -loop500 -rfbport 5900 -rfbportv6 5900 -logfile /dev/null
        4096                19734               19582               0                   Sep30               ?                   00:00:00            /usr/bin/chromedriver --port=4444 --allowed-ips=
        4096                19829               19733               0                   Sep30               ?                   00:00:25            x11vnc -display :99 -shared -forever -loop500 -rfbport 5900 -rfbportv6 5900 -logfile /dev/null

??? example "sudo docker logs <container id> --follow --timestamps"

        2021-09-28T11:03:34.381165251Z 20
        2021-09-28T11:03:34.421440407Z Waiting X server...
        2021-09-28T11:03:34.430671900Z 2021/09/28 11:03:34 [INIT] [Listening on :7070]
        2021-09-28T11:03:34.489190636Z Logging to: /dev/null
        2021-09-28T11:03:34.529555226Z Waiting X server...
        2021-09-28T11:03:34.651832620Z
        2021-09-28T11:03:34.651856370Z  --- x11vnc loop: 1 ---
        2021-09-28T11:03:34.651858875Z
        2021-09-28T11:03:34.652059233Z Starting ChromeDriver 93.0.4577.63 (ff5c0da2ec0adeaed5550e6c7e98417dac77d98a-refs/branch-heads/4577@{#1135}) on port 4444
        2021-09-28T11:03:34.652066970Z All remote connections are allowed. Use an allowlist instead!
        2021-09-28T11:03:34.652069126Z Please see https://chromedriver.chromium.org/security-considerations for suggestions on keeping ChromeDriver safe.
        2021-09-28T11:03:34.653395950Z ChromeDriver was started successfully.
        2021-09-28T11:03:34.703074472Z [1632827014.703][WARNING]: Deprecated chrome option is ignored: useAutomationExtension
        2021-09-28T11:03:34.703152156Z [1632827014.703][WARNING]: Deprecated chrome option is ignored: useAutomationExtension
        2021-09-28T11:03:35.161310861Z  --- x11vnc loop: waiting for: 145
        2021-09-28T11:03:35.161333135Z
        2021-09-28T11:03:35.305167971Z PORT=5900

### Cleanups

#### Remove unused Objects

The following call will remove all unnecessary objects and will make space free in the `/var/lib/docker/overlay2` folder.

??? example "sudo docker system prune"

        WARNING! This will remove:
          - all stopped containers
          - all networks not used by at least one container
          - all dangling images
          - all dangling build cache
        Are you sure you want to continue? [y/N] y

??? example "sudo docker system prune -a --volumes"

        WARNING! This will remove:
          - all stopped containers
          - all networks not used by at least one container
          - all volumes not used by at least one container
          - all dangling images
          - all build cache
        Are you sure you want to continue? [y/N] y

#### Delete Container

Delete all stopped containers:

??? example "docker container prune"

        WARNING! This will remove all stopped containers.
        Are you sure you want to continue? [y/N] y
        Deleted Containers:
        5d8955b86a09c93bd981c3ed2479acf2a3674c5f9b73439cd24e9f5b5777dc97

        Total reclaimed space: 0B

Remove specific container:

??? example "docker container rm <container-id>"

        <container-id>

### Compose

With the help of `docker-compose` it is possible to run multiple docker images as a package together.

{!docs/assets/abbreviations.txt!}
